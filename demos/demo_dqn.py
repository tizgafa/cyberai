import sys
import os
from typing import Dict, List, Tuple

import json
import pandas as pd
from pathlib import Path
import time

import gym
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

"""

"""
class ReplayBuffer:
    """A simple numpy replay buffer."""

    def __init__(self, obs_dim: int, size: int, batch_size: int = 32):
        self.obs_buf = np.zeros([size, obs_dim], dtype=np.float32)
        self.next_obs_buf = np.zeros([size, obs_dim], dtype=np.float32)
        self.acts_buf = np.zeros([size], dtype=np.float32)
        self.rews_buf = np.zeros([size], dtype=np.float32)
        self.done_buf = np.zeros(size, dtype=np.float32)
        self.max_size, self.batch_size = size, batch_size
        self.ptr, self.size, = 0, 0

    def store(
        self,
        obs: np.ndarray,
        act: np.ndarray, 
        rew: float, 
        next_obs: np.ndarray, 
        done: bool,
    ):
        self.obs_buf[self.ptr] = obs
        self.next_obs_buf[self.ptr] = next_obs
        self.acts_buf[self.ptr] = act
        self.rews_buf[self.ptr] = rew
        self.done_buf[self.ptr] = done
        self.ptr = (self.ptr + 1) % self.max_size
        self.size = min(self.size + 1, self.max_size)

    def sample_batch(self) -> Dict[str, np.ndarray]:
        idxs = np.random.choice(self.size, size=self.batch_size, replace=False)
        return dict(obs=self.obs_buf[idxs],
                    next_obs=self.next_obs_buf[idxs],
                    acts=self.acts_buf[idxs],
                    rews=self.rews_buf[idxs],
                    done=self.done_buf[idxs])

    def __len__(self) -> int:
        return self.size


"""
"""
class Network(nn.Module):
    def __init__(self, in_dim: int, out_dim: int):
        """Initialization."""
        super(Network, self).__init__()

        self.layers = nn.Sequential(
            nn.Linear(in_dim, 128), 
            nn.ReLU(),
            nn.Linear(128, 128), 
            nn.ReLU(), 
            nn.Linear(128, out_dim)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward method implementation."""
        return self.layers(x)

"""
"""
class DQNAgent:
    """DQN Agent interacting with environment."""
    def __init__(
        self, 
        env: gym.Env,
        memory_size: int,
        batch_size: int,
        target_update: int,
        epsilon_decay: float,
        max_epsilon: float = 1.0,
        min_epsilon: float = 0.1,
        gamma: float = 0.99,
    ):
        """Initialization. """
        obs_dim = env.observation_space.shape[0]
        action_dim = env.action_space.n
        
        self.env = env
        self.memory = ReplayBuffer(obs_dim, memory_size, batch_size)
        self.batch_size = batch_size
        self.epsilon = max_epsilon
        self.epsilon_decay = epsilon_decay
        self.max_epsilon = max_epsilon
        self.min_epsilon = min_epsilon
        self.target_update = target_update
        self.gamma = gamma
        
        # device: cpu / gpu
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )
        print(self.device)

        # networks: dqn, dqn_target
        self.dqn = Network(obs_dim, action_dim).to(self.device)
        self.dqn_target = Network(obs_dim, action_dim).to(self.device)
        self.dqn_target.load_state_dict(self.dqn.state_dict())
        self.dqn_target.eval()
        
        # optimizer
        self.optimizer = optim.Adam(self.dqn.parameters())

        # transition to store in memory
        self.transition = list()
        
        # losses
        self.train_losses = []
        self.train_scores = []

        # mode: train / test
        self.is_test = False

        self.update_cnt = 0

    def select_action(self, state: np.ndarray) -> np.ndarray:
        """Select an action from the input state."""
        # epsilon greedy policy
        if self.epsilon > np.random.random():
            selected_action = self.env.action_space.sample()
        else:
            selected_action = self.dqn(
                torch.FloatTensor(state).to(self.device)
            ).argmax()
            selected_action = selected_action.detach().cpu().numpy()
        
        if not self.is_test:
            self.transition = [state, selected_action]
        
        return selected_action

    def step(self, action: np.ndarray) -> Tuple[np.ndarray, np.float64, bool]:
        """Take an action and return the response of the env."""
        next_state, reward, done, _ = self.env.step(action)

        if not self.is_test:
            self.transition += [reward, next_state, done]
            self.memory.store(*self.transition)
    
        return next_state, reward, done

    def update_model(self) -> torch.Tensor:
        """Update the model by gradient descent."""
        samples = self.memory.sample_batch()

        loss = self._compute_dqn_loss(samples)

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss.item()
        
    def train(self, num_frames: int, plotting_interval: int = 200):
        """Train the agent."""
        self.is_test = False
        
        state = self.env.reset()
        self.update_cnt = 0
        epsilons = []
        self.train_losses = []
        self.train_scores = []
        score = 0

        for frame_idx in range(1, num_frames + 1):
            action = self.select_action(state)
            next_state, reward, done = self.step(action)

            state = next_state
            score += reward

            # if episode ends
            if done:
                state = self.env.reset()
                self.train_scores.append(score)
                score = 0

            # if training is ready
            if len(self.memory) >= self.batch_size:
                loss = self.update_model()
                self.train_losses.append(loss)
                self.update_cnt += 1
                
                # linearly decrease epsilon
                self.epsilon = max(
                    self.min_epsilon, self.epsilon - (
                        self.max_epsilon - self.min_epsilon
                    ) * self.epsilon_decay
                )
                epsilons.append(self.epsilon)
                
                # if hard update is needed
                if self.update_cnt % self.target_update == 0:
                    self._target_hard_update()

            # plotting
            if frame_idx % plotting_interval == 0:
                self._plot(frame_idx, self.train_scores, self.train_losses, epsilons)
                
        self.env.close()
                
    def test(self) -> List[np.ndarray]:
        """Test the agent."""
        self.is_test = True
        
        state = self.env.reset()
        done = False
        score = 0
        
        frames = []
        while not done:
            frames.append(self.env.render(mode="rgb_array"))
            action = self.select_action(state)
            next_state, reward, done = self.step(action)

            state = next_state
            score += reward
        
        print("score: ", score)
        self.env.close()
        
        return frames

    def _compute_dqn_loss(self, samples: Dict[str, np.ndarray]) -> torch.Tensor:
        """Return dqn loss."""
        device = self.device  # for shortening the following lines
        state = torch.FloatTensor(samples["obs"]).to(device)
        next_state = torch.FloatTensor(samples["next_obs"]).to(device)
        action = torch.LongTensor(samples["acts"].reshape(-1, 1)).to(device)
        reward = torch.FloatTensor(samples["rews"].reshape(-1, 1)).to(device)
        done = torch.FloatTensor(samples["done"].reshape(-1, 1)).to(device)

        # G_t   = r + gamma * v(s_{t+1})  if state != Terminal
        #       = r                       otherwise
        curr_q_value = self.dqn(state).gather(1, action)
        next_q_value = self.dqn_target(
            next_state
        ).max(dim=1, keepdim=True)[0].detach()
        mask = 1 - done
        target = (reward + self.gamma * next_q_value * mask).to(self.device)

        # calculate dqn loss
        loss = F.smooth_l1_loss(curr_q_value, target)

        return loss

    def _target_hard_update(self):
        """Hard update: target <- local."""
        self.dqn_target.load_state_dict(self.dqn.state_dict())
                
    def _plot(
        self, 
        frame_idx: int, 
        scores: List[float], 
        losses: List[float], 
        epsilons: List[float],
    ):
        """Plot the training progresses."""
        plt.figure(figsize=(20, 5))
        plt.subplot(131)
        plt.title('frame %s. score: %s' % (frame_idx, np.mean(scores[-10:])))
        plt.plot(scores)
        plt.subplot(132)
        plt.title('loss')
        plt.plot(losses)
        plt.subplot(133)
        plt.title('epsilons')
        plt.plot(epsilons)
        plt.show()

    def save_model(self, path: str = None):
        type_agent = 'dqn'
        env_name = 'cartpole'

        if path is None or path is True:    
            root_path = Path(os.path.abspath(__file__))
            path = os.path.join (root_path.parent.parent,"output", "demo")
        else:
            path = os.path.join(path,"output", "demo")
        path_str_latest = os.path.join(type_agent, env_name, 'latest')
        whole_path_str_latest = os.path.join(path, path_str_latest)
        
        path_str_time = os.path.join(type_agent, env_name, 'history', str(int(time.time())))
        whole_path_str_time = os.path.join(path, path_str_time)
        
        path_latest = Path(whole_path_str_latest)
        path_time = Path(whole_path_str_time)


        try:
            path_latest.mkdir(parents=True)   
            print("Created dir\n", path_str_latest)
        except FileExistsError as err:
            #print(f'Overwriting content of {err.filename}')
            pass
        try:  
            path_time.mkdir(parents=True)
            print("Created dir\n", path_str_time)
        except FileExistsError as err:
            #print(f'Overwriting content of {err.filename}')
            pass

        model_dict = {
            'epoch': 0,
            'model_state_dict': self.dqn.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
            'loss': self.train_losses
        }
        try:
            # Writing metadata
            metadata = self._get_metadata()
            with open(f'{whole_path_str_latest}/metadata.json', 'w') as json_file:
                json.dump(metadata, json_file, indent=4, sort_keys=True)
            with open(f'{whole_path_str_time}/metadata.json', 'w') as json_file:
                json.dump(metadata, json_file, indent=4, sort_keys=True)
            
            # Save model
            torch.save(model_dict, f'{whole_path_str_latest}/model.pt')
            torch.save(model_dict, f'{whole_path_str_time}/model.pt')
            
            #print("losses", self.train_losses)
            df = pd.DataFrame(self.train_losses)
            df.to_csv(f'{whole_path_str_latest}/losses.csv', index=False)
            df.to_csv(f'{whole_path_str_time}/losses.csv', index=False)
            
            #print("scores", self.train_scores)
            df = pd.DataFrame(self.train_scores)
            df.to_csv(f'{whole_path_str_latest}/scores.csv', index=False)
            df.to_csv(f'{whole_path_str_time}/scores.csv', index=False)

        except OSError as err:
            print (f'Creation of the directory {path} failed: {err}')
        # else:
        #     print ("Successfully loaded the directory %s " % path)
        
        return path
    def _get_metadata(self):
        return {
            'name': 'demo_dqn',
            'batch_size': self.batch_size,
            'gamma': self.gamma,
            'num_frames': self.update_cnt,
            'batch_size': self.batch_size,
            'target_update': self.target_update,
            'epsilon_decay': self.epsilon_decay,
            'max_epsilon': self.max_epsilon,
            'min_epsilon': self.min_epsilon,
            'memory_length': len(self.memory),
        }

"""
"""
if __name__ == "__main__":
    # environment
    env_id = "CartPole-v0"
    env = gym.make(env_id)
    # parameters
    num_frames = 1000
    memory_size = 1000
    batch_size = 32
    target_update = 100
    epsilon_decay = 1 / 2000

    agent = DQNAgent(env, memory_size, batch_size, target_update, epsilon_decay)

    agent.train(num_frames)
    agent.save_model()

    frames = agent.test()
