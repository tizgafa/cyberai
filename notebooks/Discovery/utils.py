import os
import sys

import pandas as pd

from io import StringIO
import csv

def read_data(filepath, n_rows = 10, header = None, cat_ohe=False, target = 41):
    with open(filepath, "r") as f:
        df_rows = []
        n_cols = 0
        for (i,line) in enumerate(f): 
            if i > n_rows -1 : 
                #print(f'Number of columns: ${len(row)}')
                n_cols = len(row)
                break
            row = line.strip()[0:-1].split(",")
            #print("Line{}: {}".format(i,  row ))
            df_rows.append(row)

    #print("df_rows:", df_rows)
    columns = header if header != None and len(header) == n_cols else range(n_cols)
    df = pd.DataFrame(df_rows, columns = columns)
    for col in df.columns:
        try :
            df[col] = df[col].astype(float)
        except:
            df[col] = df[col].astype('category')
            if cat_ohe == True and col!=target:
                # One hot encode 
                # Create dummies   
                y = pd.get_dummies(df[col], prefix=col)
                # add to dataframe
                df = y.join(df)
                # remove category
                del df[col]
    #print(df)
    return df

def make_output_directories(filename):
    make_directory(filename)
    make_directory(f'{filename}/figures')
    pass

def make_directory(dirPath):
    try:
        # Create target Directory
        os.mkdir(dirPath)
        print("Directory " , dirPath ,  " Created ") 
    except FileExistsError:
        print("Directory " , dirPath ,  " already exists")
