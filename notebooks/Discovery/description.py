import os
import sys

import pandas as pd

from io import StringIO
import csv
from utils import read_data, make_output_directories

def data_description(data_path, output_path='./output'):
    output_path= f'{output_path}/description_kdd99'
    df = read_data(data_path)
    make_output_directories(output_path)

    
    for col in df.columns:
        try :
            df[col] = df[col].astype(float)
        except:
            df[col] = df[col].astype('category')
            ax = df[col].value_counts().plot(kind='bar')
            fig = ax.get_figure()
            fig.savefig(f'{output_path}/figures/{col}-figure.jpg')
    
    print(df.describe(include='all'))
    

