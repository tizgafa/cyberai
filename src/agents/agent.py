import json
import os
import shutil
import time
from typing import List, Tuple

from environments.environment import Environment
from networks.dqn_network import Network

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import torch
import torch.nn.functional as F
import torch.optim as optim

class Agent:
    def __init__(
        self, 
        env: Environment,
        batch_size: int,
        gamma: float = 0.99,
    ):
        
        # obs_dim = env.observation_space.shape[0]
        # action_dim = env.action_space.n
        # obs_dim = env.obs_dim
        # action_dim = env.action_space.n

        self.env = env
        self.batch_size = batch_size
        self.gamma = gamma

        # device: cpu / gpu -> torch.cuda.is_available()
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )
        print(self.device)

        # losses
        self.train_losses = []
        self.train_scores = []

        # transition to store in memory
        self.transition = list()
        
        # mode: train / test
        self.is_test = False

        self.update_cnt = 0


    def select_action(self, state: np.ndarray) -> np.ndarray:
      """Select an action from the input state."""
      # epsilon greedy policy
      pass

    def step(self, action: np.ndarray, state: np.ndarray = None) -> Tuple[np.ndarray, np.float64, bool]:
        """
            Take an (action, state) pair,
            Saves transition in `self.transition` 
            Returns the response of the env.
        """
        next_state, reward, done, _ = self.env.step(action)
        self.transition = [state, action, reward, next_state, done]
        #self.memory.store(state, action, reward, next_state, done)

        return next_state, reward, done

    def update_model(self) -> torch.Tensor:
        """Update the model by gradient descent."""
        pass
        
    def train(self, num_frames: int):
        """Train the agent."""
        pass

    def _construct_path(self):
        root_path = Path(os.path.abspath(__file__))
        #return os.path.join (root_path.parent.parent.parent,"output")
        return root_path.parent.parent.parent

    def save_model(self, type_agent: str = None, path: str = None):
        type_agent = self.name
        if path is None or path is True:
            path = os.path.join (self._construct_path(),"output")
        else:
            path = os.path.join(path,"output")
        
        path_str_latest = os.path.join(type_agent, self.env.name, 'latest')
        whole_path_str_latest = os.path.join(path, path_str_latest)
        
        path_str_time = os.path.join(type_agent, self.env.name, 'history', str(int(time.time())))
        whole_path_str_time = os.path.join(path, path_str_time)
        
        path_latest = Path(whole_path_str_latest)
        path_time = Path(whole_path_str_time)


        try:
            path_latest.mkdir(parents=True)   
            print("Created dir\n", path_str_latest)
        except FileExistsError as err:
            #print(f'Overwriting content of {err.filename}')
            pass
        try:  
            path_time.mkdir(parents=True)
            print("Created dir\n", path_str_time)
        except FileExistsError as err:
            #print(f'Overwriting content of {err.filename}')
            pass

        model_dict = {
            'epoch': 0,
            'model_state_dict': self.dqn.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
            'loss': self.train_losses
        }
        try:
            # Writing metadata
            metadata = self._get_metadata()
            with open(f'{whole_path_str_latest}/metadata.json', 'w') as json_file:
                json.dump(metadata, json_file, indent=4, sort_keys=True)
            with open(f'{whole_path_str_time}/metadata.json', 'w') as json_file:
                json.dump(metadata, json_file, indent=4, sort_keys=True)
            
            # Save model
            torch.save(model_dict, f'{whole_path_str_latest}/model.pt')
            torch.save(model_dict, f'{whole_path_str_time}/model.pt')
            
            #print("losses", self.train_losses)
            df = pd.DataFrame(self.train_losses)
            df.to_csv(f'{whole_path_str_latest}/losses.csv', index=False)
            df.to_csv(f'{whole_path_str_time}/losses.csv', index=False)
            
            #print("scores", self.train_scores)
            df = pd.DataFrame(self.train_scores)
            df.to_csv(f'{whole_path_str_latest}/scores.csv', index=False)
            df.to_csv(f'{whole_path_str_time}/scores.csv', index=False)

            # real and pred
            real, pred = self.env.get_real_pred()
            # Save real values
            df = pd.DataFrame(list(zip(real,pred)),columns=["real","pred"])
            df.to_csv(f'{whole_path_str_latest}/real_pred.csv', index=False)
            df.to_csv(f'{whole_path_str_time}/real_pred.csv', index=False)

        except OSError as err:
            print (f'Creation of the directory {path} failed: {err}')
        # else:
        #     print ("Successfully loaded the directory %s " % path)
        
        return path

    def load_model(self, path=None):
        if path is None:
            #self._construct_path()
            path = os.path.join(
                self._construct_path(),
                'output',
                self.name,
                self.env.name,
                'latest',
                'model.pt'
            )
        # networks: dqn, dqn_target
        self.dqn = Network(self.env.obs_dim, self.env.action_space.n).to(self.device)
        self.dqn_target = Network(self.env.obs_dim, self.env.action_space.n).to(self.device)

        checkpoint = torch.load(path)
        try:
            self.dqn.load_state_dict(checkpoint['model_state_dict'])
        except:
            print("Loaded model and network do no match.\nWARNING: No model loaded.")
            return
        self.dqn.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        #epoch = 0 #checkpoint['epoch']
        self.train_losses = checkpoint['loss']

        self.dqn_target.load_state_dict(self.dqn.state_dict())
        
        self.dqn.eval()
        self.dqn_target.eval()

        print("Model correctly loaded from:", path)
        
                
    def test(self) -> List[np.ndarray]:
        """Test the agent."""
        self.is_test = True
        self.env.set_test_data(100)
        print(self.env.obs)
        state = self.env.reset()
        done = False
        score = 0
        self.train_scores = []
        
        frames = []
        while not done:
            frames.append(self.env.render())
            action = self.select_action(state)
            next_state, reward, done = self.step(action, state)

            state = next_state
            score += reward
        
        print("score: ", score)
        self.env.close()
        
        return frames
    
    def _get_metadata(self):
        return {
            'name': self.env.name,
            'batch_size': self.batch_size,
            'gamma': self.gamma,
            'num_frames': self.update_cnt,
            'total_obs': self.env.total_obs,
        }