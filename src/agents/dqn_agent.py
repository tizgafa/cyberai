import os
import shutil
import time
from typing import Dict, List, Tuple

from environments.environment import Environment
from data_buffer.simple_replay_buffer import ReplayBuffer
from networks.dqn_network import Network
from agents.agent import Agent


import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
from tqdm import tqdm

class DQNAgent(Agent):
    def __init__(
        self, 
        env: Environment,
        memory_size: int,
        batch_size: int,
        target_update: int,
        epsilon_decay: float,
        max_epsilon: float = 1.0,
        min_epsilon: float = 0.1,
        gamma: float = 0.99,
    ):
        # Init environment, batchsize, gamma, torch.device, Networks
        super().__init__(env, batch_size, gamma)
        self.name = 'dqn'
        # obs_dim = env.obs_dim
        # action_dim = env.action_space.n
        
        #self.env = env
        #self.batch_size = batch_size
        #self.gamma = gamma
        self.memory = ReplayBuffer(self.env.obs_dim, memory_size, batch_size)
        self.epsilon = max_epsilon
        self.epsilon_decay = epsilon_decay
        self.max_epsilon = max_epsilon
        self.min_epsilon = min_epsilon
        self.target_update = target_update

        # losses
        # self.train_losses = []
        # self.train_scores = []

        # device: cpu / gpu -> torch.cuda.is_available()
        # self.device = torch.device(
        #     "cuda" if False else "cpu"
        # )
        # print(self.device)

        # networks: dqn, dqn_target
        self.dqn = Network(self.env.obs_dim, self.env.action_space.n).to(self.device)
        self.dqn_target = Network(self.env.obs_dim, self.env.action_space.n).to(self.device)
        self.dqn_target.load_state_dict(self.dqn.state_dict())
        self.dqn_target.eval()
        
        # optimizer
        self.optimizer = optim.Adam(self.dqn.parameters())

        # transition to store in memory
        self.transition = list()
        

    def select_action(self, state: np.ndarray) -> np.ndarray:
        """Select an action from the input state."""
        # epsilon greedy policy
        if self.epsilon > np.random.random():
            selected_action = self.env.action_space.sample()
        else:
            selected_action = self.dqn(
                torch.FloatTensor(state).to(self.device)
            ).argmax()
            selected_action = selected_action.detach().cpu().numpy()
        
        return selected_action

    def step(self, action: np.ndarray, state: np.ndarray = None) -> Tuple[np.ndarray, np.float64, bool]:
        """Take an action and return the response of the env."""
        next_state, reward, done = super().step(action, state)
        if not self.is_test:
            self.memory.store(*self.transition)

        return next_state, reward, done

    def update_model(self) -> torch.Tensor:
        """Update the model by gradient descent."""
        samples = self.memory.sample_batch()

        loss = self._compute_dqn_loss(samples)

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss.item()
        
    def train(self, num_frames: int, plotting_interval: int = 200):
        """Train the agent."""
        self.is_test = False
        
        state = self.env.reset()
        self.update_cnt = 0
        epsilons = []
        #losses = []
        #scores = []
        score = 0

        for frame_idx in tqdm(range(1, num_frames + 1)):
            selected_action = self.select_action(state)
            next_state, reward, done = self.step(selected_action, state)
            
            state = next_state
            score += reward

            # if episode ends
            if done:
                state = self.env.reset()
                self.train_scores.append(score)
                score = 0

            # if training is ready
            if len(self.memory) >= self.batch_size:
                loss = self.update_model()
                self.train_losses.append(loss)
                self.update_cnt += 1
                
                # linearly decrease epsilon
                self.epsilon = max(
                    self.min_epsilon, self.epsilon - (
                        self.max_epsilon - self.min_epsilon
                    ) * self.epsilon_decay
                )
                epsilons.append(self.epsilon)
                
                # if hard update is needed
                if self.update_cnt % self.target_update == 0:
                    self._target_hard_update()
            # plotting
            # if frame_idx % plotting_interval == 0:
            #     self._plot(frame_idx, self.train_scores, self.train_losses, epsilons)

        print("Closing...")    
        self.env.close()
        
        return self.train_scores, self.train_losses


    def _compute_dqn_loss(self, samples: Dict[str, np.ndarray]) -> torch.Tensor:
        """Return dqn loss."""
        device = self.device  # for shortening the following lines
        state = torch.FloatTensor(samples["obs"]).to(device)
        next_state = torch.FloatTensor(samples["next_obs"]).to(device)
        action = torch.LongTensor(samples["acts"].reshape(-1, 1)).to(device)
        reward = torch.FloatTensor(samples["rews"].reshape(-1, 1)).to(device)
        done = torch.FloatTensor(samples["done"].reshape(-1, 1)).to(device)

        # G_t   = r + gamma * v(s_{t+1})  if state != Terminal
        #       = r                       otherwise
        curr_q_value = self.dqn(state).gather(1, action)
        next_q_value = self.dqn_target(next_state).max(dim=1, keepdim=True)[0].detach()
        mask = 1 - done
        target = (reward + self.gamma * next_q_value * mask).to(self.device)

        # calculate dqn loss
        loss = F.smooth_l1_loss(curr_q_value, target)

        return loss

    def _target_hard_update(self):
        """Hard update: target <- local."""
        self.dqn_target.load_state_dict(self.dqn.state_dict())

    def _get_metadata(self):
        metadata = super()._get_metadata()

        a = {
            'batch_size': self.batch_size,
            'target_update': self.target_update,
            'epsilon_decay': self.epsilon_decay,
            'max_epsilon': self.max_epsilon,
            'min_epsilon': self.min_epsilon,
            'gamma': self.gamma,
            'memory_length': len(self.memory)
        }
        return dict(metadata, **a)
    def _plot(
        self, 
        frame_idx: int, 
        scores: List[float], 
        losses: List[float], 
        epsilons: List[float],
    ):
        """Plot the training progresses."""
        plt.figure(figsize=(20, 5))
        plt.subplot(131)
        plt.title('frame %s. score: %s' % (frame_idx, np.mean(scores[-10:])))
        plt.plot(scores)
        plt.subplot(132)
        plt.title('loss')
        plt.plot(losses)
        plt.subplot(133)
        plt.title('epsilons')
        plt.plot(epsilons)
        plt.show()

"""
def main():
    # environment
    env_id = "CartPole-v0"
    env = gym.make(env_id)
    num_frames = 20000
    memory_size = 1000
    batch_size = 32
    target_update = 100
    epsilon_decay = 1 / 2000

    agent = DQNAgent(env, memory_size, batch_size, target_update, epsilon_decay)
"""