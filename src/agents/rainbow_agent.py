import os
from typing import Dict, List, Tuple

from environments.environment import Environment
from data_buffer.replay_buffer import ReplayBuffer
from data_buffer.prioritized import PrioritizedReplayBuffer
from networks.rainbow_network import Network
from agents.agent import Agent

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
from torch.nn.utils import clip_grad_norm_

from tqdm import tqdm

class DQNAgent(Agent):
    """DQN Agent interacting with environment.
    
    Attribute:
        env (Environment): General Environment
        memory (PrioritizedReplayBuffer): replay memory to store transitions
        batch_size (int): batch size for sampling
        target_update (int): period for target model's hard update
        gamma (float): discount factor
        dqn (Network): model to train and select actions
        dqn_target (Network): target model to update
        optimizer (torch.optim): optimizer for training dqn
        transition (list): transition information including 
                           state, action, reward, next_state, done
        v_min (float): min value of support
        v_max (float): max value of support
        atom_size (int): the unit number of support
        support (torch.Tensor): support for categorical dqn
        use_n_step (bool): whether to use n_step memory
        n_step (int): step number to calculate n-step td error
        memory_n (ReplayBuffer): n-step replay buffer
    """

    def __init__(
        self, 
        env: Environment,
        memory_size: int,
        batch_size: int,
        target_update: int,
        gamma: float = 0.99,
        # PER parameters
        alpha: float = 0.2,
        beta: float = 0.6,
        prior_eps: float = 1e-6,
        # Categorical DQN parameters
        v_min: float = 0.0,
        v_max: float = 200.0,
        atom_size: int = 51,
        # N-step Learning
        n_step: int = 1,
    ):
        super(DQNAgent, self).__init__(env, batch_size, gamma)
        # self.env = env
        # self.batch_size = batch_size
        # self.gamma = gamma
        self.target_update = target_update
        self.name = 'rainbow'

        # NoisyNet: All attributes related to epsilon are removed
        
        # PER
        # memory for 1-step Learning
        self.alpha = alpha
        self.beta = beta
        self.prior_eps = prior_eps
        self.memory = PrioritizedReplayBuffer(
            self.env.obs_dim, memory_size, batch_size, alpha=alpha
        )
        
        # memory for N-step Learning

        self.n_step = n_step
        self.use_n_step = True if n_step > 1 else False
        if self.use_n_step:
            self.n_step = n_step
            self.memory_n = ReplayBuffer(
                self.env.obs_dim, memory_size, batch_size, n_step=n_step, gamma=gamma
            )
            
        # Categorical DQN parameters
        self.v_min = v_min
        self.v_max = v_max
        self.atom_size = atom_size
        self.support = torch.linspace(
            self.v_min, self.v_max, self.atom_size
        ).to(self.device)

        # networks: dqn, dqn_target
        self.dqn = Network(
            self.env.obs_dim, self.env.action_space.n, self.atom_size, self.support
        ).to(self.device)
        self.dqn_target = Network(
            self.env.obs_dim, self.env.action_space.n, self.atom_size, self.support
        ).to(self.device)
        self.dqn_target.load_state_dict(self.dqn.state_dict())
        self.dqn_target.eval()
        
        # optimizer
        self.optimizer = optim.Adam(self.dqn.parameters())


    def select_action(self, state: np.ndarray) -> np.ndarray:
        """Select an action from the input state."""
        # NoisyNet: no epsilon greedy action selection
        selected_action = self.dqn(
            torch.FloatTensor(state).to(self.device)
        ).argmax()
        selected_action = selected_action.detach().cpu().numpy()
        
        # if not self.is_test:
        #     self.transition = [state, selected_action]
        
        return selected_action

    def step(self, action: np.ndarray, state: np.ndarray = None) -> Tuple[np.ndarray, np.float64, bool]:
        """Take an action and return the response of the env."""
        
        next_state, reward, done, _ = self.env.step(action)
        self.transition = [state, action, reward, next_state, done]
        
        if not self.is_test:
            # N-step transition
            if self.use_n_step:
                one_step_transition = self.memory_n.store(*self.transition)
            # 1-step transition
            else:
                one_step_transition = self.transition

            # add a single step transition
            if one_step_transition:
                self.memory.store(*one_step_transition)

        return next_state, reward, done

    def update_model(self) -> torch.Tensor:
        """Update the model by gradient descent."""
        # PER needs beta to calculate weights
        samples = self.memory.sample_batch(self.beta)
        weights = torch.FloatTensor(
            samples["weights"].reshape(-1, 1)
        ).to(self.device)
        indices = samples["indices"]
        
        # 1-step Learning loss
        elementwise_loss = self._compute_dqn_loss(samples, self.gamma)
        
        # PER: importance sampling before average
        loss = torch.mean(elementwise_loss * weights)
        
        # N-step Learning loss
        # we are gonna combine 1-step loss and n-step loss so as to
        # prevent high-variance. The original rainbow employs n-step loss only.
        if self.use_n_step:
            gamma = self.gamma ** self.n_step
            samples = self.memory_n.sample_batch_from_idxs(indices)
            elementwise_loss_n_loss = self._compute_dqn_loss(samples, gamma)
            elementwise_loss += elementwise_loss_n_loss
            
            # PER: importance sampling before average
            loss = torch.mean(elementwise_loss * weights)

        self.optimizer.zero_grad()
        loss.backward()
        clip_grad_norm_(self.dqn.parameters(), 10.0)
        self.optimizer.step()
        
        # PER: update priorities
        loss_for_prior = elementwise_loss.detach().cpu().numpy()
        new_priorities = loss_for_prior + self.prior_eps
        self.memory.update_priorities(indices, new_priorities)
        
        # NoisyNet: reset noise
        self.dqn.reset_noise()
        self.dqn_target.reset_noise()

        return loss.item()
        
    def train(self, num_frames: int, plotting_interval: int = 200):
        """Train the agent."""
        self.is_test = False
        
        state = self.env.reset()
        self.update_cnt = 0
        self.train_losses = []
        self.train_scores = []
        score = 0

        for frame_idx in tqdm(range(1, num_frames + 1)):
            # print("idx",frame_idx)
            action = self.select_action(state)
            next_state, reward, done = self.step(action, state)

            state = next_state
            score += reward
            
            # NoisyNet: removed decrease of epsilon
            
            # PER: increase beta
            fraction = min(frame_idx / num_frames, 1.0)
            self.beta = self.beta + fraction * (1.0 - self.beta)

            # if episode ends NOT APPLICABLE
            if done:
                state = self.env.reset()
                self.train_scores.append(score)
                score = 0

            # if training is ready
            if len(self.memory) >= self.batch_size:
                loss = self.update_model()
                self.train_losses.append(loss)
                self.update_cnt += 1
                
                # if hard update is needed
                if self.update_cnt % self.target_update == 0:
                    self._target_hard_update()
                # import IPython; IPython.embed();exit(1)

            # plotting WORKS WORSE if `_plot()` function empty
            # if frame_idx % plotting_interval == 0:
            #     self._plot(frame_idx, self.train_scores, self.train_losses)
                
        self.env.close()

    def test(self):
        """Test the agent."""
        self.is_test = True
        # self.env.set_test_data(100)
        print(self.env.obs)
        state = self.env.reset()
        done = False
        score = 0
        self.train_scores = []
        
        frames = []
        while not done:
            frames.append(self.env.render())
            action = self.select_action(state)
            next_state, reward, done = self.step(action, state)

            state = next_state
            self.train_scores.append(reward)
            score += reward
        
        print("score: ", score)
        self.env.close()
        
        # return frames

    def load_model(self, path = "output/latest/rainbow.pt"):
         # networks: dqn, dqn_target
        self.dqn = Network(
            self.env.obs_dim,
            self.env.action_space.n,
            self.atom_size,
            self.support
        ).to(self.device)
        self.dqn_target = Network(
            self.env.obs_dim,
            self.env.action_space.n,
            self.atom_size,
            self.support
        ).to(self.device)

        checkpoint = torch.load(path)
        try:
            self.dqn.load_state_dict(checkpoint['model_state_dict'])
        except:
            print("Loaded model and network do no match.\nWARNING: No model loaded.")
            return
        self.dqn.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        #epoch = 0 #checkpoint['epoch']
        self.train_losses = checkpoint['loss']

        self.dqn_target.load_state_dict(self.dqn.state_dict())
        
        self.dqn.eval()
        self.dqn_target.eval()

        print("Model correctly loaded from:", path)


    def _compute_dqn_loss(self, samples: Dict[str, np.ndarray], gamma: float) -> torch.Tensor:
        """Return categorical dqn loss."""
        device = self.device  # for shortening the following lines
        state = torch.FloatTensor(samples["obs"]).to(device)
        next_state = torch.FloatTensor(samples["next_obs"]).to(device)
        action = torch.LongTensor(samples["acts"]).to(device)
        reward = torch.FloatTensor(samples["rews"].reshape(-1, 1)).to(device)
        done = torch.FloatTensor(samples["done"].reshape(-1, 1)).to(device)
        
        # Categorical DQN algorithm
        delta_z = float(self.v_max - self.v_min) / (self.atom_size - 1)

        with torch.no_grad():
            # Double DQN
            next_action = self.dqn(next_state).argmax(1)
            next_dist = self.dqn_target.dist(next_state)
            next_dist = next_dist[range(self.batch_size), next_action]

            t_z = reward + (1 - done) * gamma * self.support
            t_z = t_z.clamp(min=self.v_min, max=self.v_max)
            b = (t_z - self.v_min) / delta_z
            l = b.floor().long()
            u = b.ceil().long()

            offset = (
                torch.linspace(
                    0, (self.batch_size - 1) * self.atom_size, self.batch_size
                ).long()
                .unsqueeze(1)
                .expand(self.batch_size, self.atom_size)
                .to(self.device)
            )

            
            proj_dist = torch.zeros(next_dist.size(), device=self.device)
            proj_dist.view(-1).index_add_(
                0, (l + offset).view(-1), (next_dist * (u.float() - b)).view(-1)
            )
            proj_dist.view(-1).index_add_(
                0, (u + offset).view(-1), (next_dist * (b - l.float())).view(-1)
            )

        dist = self.dqn.dist(state)
        log_p = torch.log(dist[range(self.batch_size), action])
        elementwise_loss = -(proj_dist * log_p).sum(1)

        return elementwise_loss

    def _target_hard_update(self):
        """Hard update: target <- local."""
        self.dqn_target.load_state_dict(self.dqn.state_dict())

    def _get_metadata(self):
        metadata = super(DQNAgent, self)._get_metadata()
        a = {
            'target_update': self.target_update,
            'gamma': self.gamma,
            # PER parameters
            'alpha': self.alpha,
            'beta': self.beta,
            'prior_eps': self.prior_eps,
            # Categorical DQN parameters
            'v_min': self.v_min,
            'v_max': self.v_max,
            'atom_size': self.atom_size,
            # N-step Learning
            'n_step': self.n_step,
        }

        return dict(metadata, **a)

    def _plot(
        self, 
        frame_idx: int, 
        scores: List[float], 
        losses: List[float],
    ):
        """Plot the training progresses."""
        plt.figure(figsize=(20, 5))
        plt.subplot(131)
        plt.title('frame %s. score: %s' % (frame_idx, np.mean(scores[-10:])))
        plt.plot(scores)
        plt.subplot(132)
        plt.title('loss')
        plt.plot(losses)
        plt.show()