from typing import Dict, List, Tuple
from random import randint

class ActionSpace:
  """
  docstring
  """
  def __init__(self, n):
    self.n = n # Number of actions
    #self.space = space

  def sample(self):
    return randint(0,self.n - 1)

class Environment:
  def __init__(self):
    self.obs_dim = None
    self.action_dim = 0
    self.action_space = None
    self.name = ""

  def step(self, action):
    """
    Returns: next_state, reward, done, _(something else)
    """
    pass
  
  def reset(self):
    pass
  
  def set_test_data(self, total_obs):
    pass
  
  def close(self):
    pass
  
  def render(self):
    pass
