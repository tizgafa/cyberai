from environments.environment import Environment, ActionSpace
import os
import pandas as pd
from pathlib import Path
import random, math

class KDD99(Environment):
  def __init__(self, total_obs=100, states_per_episode = 10, skiprows: int=0):
    """
    docstring
    """
    super().__init__()
    
    self.name = 'kdd99'
    # train/val lengths
    self.total_obs = total_obs
    # self.train_len = int(total_obs * .9)
    # self.val_len = total_obs - self.train_len
    self.target_col = 'attack' 
    self.obs_state_cols = None
    # torch.FloatTensor(a)
    
    # Other For MEtrics
    self._predicted_attacks = []
    self._actual_attacks = []

    # Import data K99 (Only training)
    root_path = Path(__file__).parent.parent.parent.parent
    path =  root_path/'data'/'kdd_cup'/'KDD_Data_10pc'/'kddcup_data_10_percent.csv'
    self._preprocess_K99(total_obs=self.total_obs, path=path, skiprows=skiprows)
    
    assert self.obs_state_cols is not None, "self.obs_state_cols must be diferent than None"
    self.obs_dim = len(self.obs_state_cols) # dimension States (columns_model)

    self.action_space = ActionSpace(len(self._get_attack_types_array()))
    
    self.obs_ptr = None
    self.states_per_episode = states_per_episode
    self.episode_done = True
    #print("Obs: ", self.obs.head())
  
  def get_real_pred(self):
    return self._actual_attacks, self._predicted_attacks

  def calculate_reward(self, action, state_ptr):
    #print(f'{self._get_atack_types()[action]} == {self.obs.iloc[state_ptr,41]} : {str(self._get_atack_types()[action]) == str(self.obs.iloc[state_ptr,41])}')
    
    pred_attack = str(self._get_attack_types_array()[action])
    real_attack = str(self.obs[self.target_col].iloc[state_ptr])
    real_attack = self._get_atacks()[real_attack]
    
    self._predicted_attacks.append(pred_attack)
    self._actual_attacks.append(real_attack)
    
    reward = -1
    if pred_attack == real_attack:
      if pred_attack == 'normal':
        # Less reward if its normal
        reward = 1
      else:
        reward = 1
    else:
      # If we are looking at attack (not attack types) then 
      # if 'normal' not in [str(self.obs.iloc[state_ptr,42]), str(self._get_atack_types()[action]) ]:
      #   reward = -1
      # else:
      #   reward = -0.75
      reward = -1
      #print(f'Reward: {reward} .. {str(self._get_atack_types()[action])} == {str(self.obs.iloc[state_ptr,42])} -> {str(self._get_atack_types()[action]) == str(self.obs.iloc[state_ptr,42])}')
    return reward
    
  def step(self, action):
    """
    Returns: next_state, reward, done
    """
    if self.episode_done:
      return None, 0, True, None

    next_state = None
    done = False

    reward = self.calculate_reward(action, self.obs_ptr)

    #print(f'{self.obs_ptr} % {self.states_per_episode} ==  {(self.states_per_episode -1)} = {self.obs_ptr % self.states_per_episode == (self.states_per_episode -1)}')
    # Last state of episode
    if self.obs_ptr % self.states_per_episode == (self.states_per_episode -1):
      self.episode_done = True
      next_ptr = (self.obs_ptr+1 ) % self.total_obs
      next_state = self.obs[self.obs_state_cols].iloc[next_ptr, :]
      #next_state = None
      #print(f'(state={self.obs_ptr}, nxt_state={self.obs_ptr+1}, reward={reward}, {self.episode_done}, None)')
      
    else:
      # Update to new state in episode
      #print(f'(state={self.obs_ptr}, nxt_state={self.obs_ptr+1}, reward={reward}, {self.episode_done}, None)') 
      try:
        self.obs_ptr += 1
        next_state = self.obs[self.obs_state_cols].iloc[self.obs_ptr, :]
      except:
        print("Error out o bounce", len(self.obs))
        print("Length observations", len(self.obs))
        print("pointer", self.obs_ptr)
        return next_state, 0, True, None
    # if self.self.obs[self.obs_ptr]['target'] == action: reward += 1
    return next_state, reward, self.episode_done, None
  
  def set_test_data(self, total_obs: int):
    # path =
    # self.obs = self._preprocess_K99(total_obs=total_obs, train=False, path)
    pass
  def reset(self):
    """
    Returns: initial_state
    """

    self.episode_done = False
    

    if self.obs_ptr is not None:
      current_episode = int(self.obs_ptr /  self.states_per_episode)
      self.obs_ptr = int(self.states_per_episode + current_episode*self.states_per_episode)
    else:
      # If episode consisting of one step && ptr=0
      self.obs_ptr = 0


    #print(f'state ptr ={self.obs_ptr}')
    if self.obs_ptr + self.states_per_episode > self.total_obs:
      # if the last batch doest have enough start again
      print("RELOOP")
      self.obs_ptr = 0

    #print(f'Updated {self.obs_ptr}')

    return self.obs[self.obs_state_cols].iloc[self.obs_ptr, :]
  
  def render(self):
    return None
  
  def close(self):
    pass

  def _preprocess_K99(self, total_obs: int, path: Path, train: bool=True, skiprows: int=0):
    # Import data K99
    if train:
      obs = pd.read_csv(
        path,
        skiprows=skiprows,
        nrows= total_obs,
        header=None,
        names=[*self._get_ordered_columns(), self.target_col]
      )
      # import IPython; IPython.embed();exit(1)
      # obs.dropna()
      
      # OneHotEncoding categorical columns
      for cat_col in self._get_categorical_columns():
        obs = pd.concat([obs, pd.get_dummies(obs[cat_col], prefix=cat_col)],axis=1)
        obs.drop([cat_col],axis=1, inplace=True)
      # Normalizing continuous columns
      self.std_devs = obs[self._get_continuous_columns()].std()
      print("Standard deveation of training data:", self.std_devs)
      self.means = obs[self._get_continuous_columns()].mean()
      print("Mean of training data:", self.means)
      for cont_col in self._get_continuous_columns():
        if math.fabs(self.std_devs[cont_col]) < 1e-6: 
          obs[cont_col] = (obs[cont_col] - self.means[cont_col])
        else:
          obs[cont_col] = (obs[cont_col] - self.means[cont_col]) / self.std_devs[cont_col]
      obs[self.target_col] = obs[self.target_col].iloc[:].apply(lambda r: r[:-1])
      # import IPython; IPython.embed();exit(1)


      #self.obs[41]=self.obs[41].astype(str)
      # obs[41] = obs.iloc[:,41].apply(lambda r: r[:-1])   
      # obs['Attack Type'] = obs.iloc[:,41].apply(lambda r:attacks_types[r])
    # else:
      # # Validation Data
      # num = int(random.random()*(TOTAL_ROWS-total_obs))
      # obs = pd.read_csv(
      #   path,
      #   skiprows=[*range(num), *range(num+total_obs,TOTAL_ROWS)],
      #   header=None
      # )
      # obs[41] = obs.iloc[:,41].apply(lambda r: r[:-1])   
      # # obs['Attack Type'] = obs.iloc[:,41].apply(lambda r:attacks_types[r])
      

    obs.dropna()
    self.obs = obs
    self.obs_state_cols = list(obs.columns)
    self.obs_state_cols.remove(self.target_col) 

  @staticmethod
  def _get_ordered_columns():
    return ([
      'duration','protocol_type','service','flag','src_bytes','dst_bytes',
      'land','wrong_fragment','urgent','hot','num_failed_logins','logged_in',
      'num_compromised','root_shell','su_attempted','num_root','num_file_creations',
      'num_shells','num_access_files','num_outbound_cmds','is_host_login',
      'is_guest_login','count','srv_count','serror_rate','srv_serror_rate',
      'rerror_rate','srv_rerror_rate','same_srv_rate','diff_srv_rate','srv_diff_host_rate',
      'dst_host_count','dst_host_srv_count','dst_host_same_srv_rate',
      'dst_host_diff_srv_rate','dst_host_same_src_port_rate',
      'dst_host_srv_diff_host_rate','dst_host_serror_rate',
      'dst_host_srv_serror_rate','dst_host_rerror_rate','dst_host_srv_rerror_rate'
    ])
  @staticmethod
  def _get_categorical_columns():
      return ['protocol_type', 'service', 'flag']
  @staticmethod
  def _get_continuous_columns():
      return [
        'duration','src_bytes','dst_bytes','wrong_fragment',
        'urgent','hot', 'num_failed_logins','num_compromised','num_root','num_file_creations',
        'num_shells','num_access_files','num_outbound_cmds', 'count','srv_count','serror_rate',
        'srv_serror_rate','rerror_rate','srv_rerror_rate','same_srv_rate','diff_srv_rate',
        'srv_diff_host_rate','dst_host_count','dst_host_srv_count','dst_host_same_srv_rate',
        'dst_host_diff_srv_rate','dst_host_same_src_port_rate','dst_host_srv_diff_host_rate',
        'dst_host_serror_rate', 'dst_host_srv_serror_rate','dst_host_rerror_rate','dst_host_srv_rerror_rate'
      ]
  @staticmethod
  def _get_discrete_columns():
      return ['land', 'logged_in','root_shell','su_attempted', 'is_host_login','is_guest_login']

  # def _get_all_columns():
  #     return [*cat_cols, *cont_cols, *disc_cols]
  @staticmethod
  def _get_labels():
    return ['attack', 'attack_type']
  
  @staticmethod
  def _get_atacks():
    return {
      'normal': 'normal',
      'back': 'dos',
      'buffer_overflow': 'u2r',
      'ftp_write': 'r2l',
      'guess_passwd': 'r2l',
      'imap': 'r2l',
      'ipsweep': 'probe',
      'land': 'dos',
      'loadmodule': 'u2r',
      'multihop': 'r2l',
      'neptune': 'dos',
      'nmap': 'probe',
      'perl': 'u2r',
      'phf': 'r2l',
      'pod': 'dos',
      'portsweep': 'probe',
      'rootkit': 'u2r',
      'satan': 'probe',
      'smurf': 'dos',
      'spy': 'r2l',
      'teardrop': 'dos',
      'warezclient': 'r2l',
      'warezmaster': 'r2l',
    }

  @staticmethod
  def _get_attack_types_dict():
    return {
      'normal': 0,
      'dos': 1,
      'u2r': 2,
      'r2l': 3,
      'probe': 4
    }

  @staticmethod
  def _get_attack_types_array():
    return ['normal','dos','u2r','r2l','probe']