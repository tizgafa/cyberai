from typing import Dict, List, Tuple, Any
import numpy as np

import gym
from environments.environment import Environment, ActionSpace

class CartPole(Environment):
  def __init__(self):
    """
    docstring
    """
    super().__init__()
    self.name = 'cartpole'
    self.env = gym.make("CartPole-v0")
    # obs_dim = env.observation_space.shape[0]
    # action_dim = env.action_space.n
    self.obs_dim = self.env.observation_space.shape[0]
    self.action_space = ActionSpace(self.env.action_space.n)
    
    
  def step(self, action) -> Tuple[np.ndarray, np.float64, bool, Any]:
    return self.env.step(action)

  def reset(self):
    return self.env.reset()
  
  def render(self):
    return self.env.render(mode="rgb_array")
  
  def close(self):
    self.env.close()