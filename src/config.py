import argparse

import os
import sys
import json

def retrieve_config():
    # Create the parser
    my_parser = argparse.ArgumentParser(
        prog = 'cyber-ai',
        description='Cli to execute/train/test RL Rainbow algorithms',
        usage='%(prog)s [options] command'
    )
    my_parser.add_argument(
        '-d',
        '--data',
        metavar='file',
        type=str,
        help='path to data file'
    )
    
    my_parser.add_argument(
        '-a','--agent', type=str,
        #required=True,
        help='Select Agent ("dqn", "rainbow")'
    )
    my_parser.add_argument(
        '-e','--env', type=str,
        #required=True,
        help='Select Environment ("cartpole", "kdd99")'
    )

    subparsers = my_parser.add_subparsers(dest='command', required=True)
    
    #parser_description = subparsers.add_parser('description', help='a help')
    #parser_description.add_argument('bar', type=int, help='bar help')
    
    # Train
    parser_train = subparsers.add_parser(
        'train',
        help='Train the model in the environment selected'
    )

    # Test
    parser_test = subparsers.add_parser(
        'test',
        help='Test the model in the environment selected'
    )
    # train --save
    parser_train.add_argument(
        '-s', '--save',
        metavar='file',
        type=str,
        nargs='?',
        const = True, # When arg is given with no value
        default=None, # When no arg is given
        help='Save output to file'
    )
    # test --model
    parser_test.add_argument(
        '-m', '--model',
        metavar='file',
        type=str,
        nargs='?',
        const = True, # When arg is given with no value
        default=None, # When no arg is given (-m not present)
        help='Model to use (Path to load the model)'
    )

    # Execute the parse_args() method
    args = my_parser.parse_args()

    print(args)
    save = None
    if args.command == 'train':
        save = args.save

    model = None
    if args.command == 'test':
        model = args.model
    
    config = {
        'data': args.data,
        'command': args.command,
        'env': args.env,
        'agent': args.agent,
        'save': save,
        'model': model,

    }
    
    print(json.dumps(config, indent=4, sort_keys=True))
    #sys.exit()
    return config
