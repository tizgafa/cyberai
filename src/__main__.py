import sys
from random import randint

from agents import dqn_agent
from agents import rainbow_agent
from config import retrieve_config
from environments.cartpole import CartPole 
from environments.kdd99 import KDD99


if __name__ == "__main__":
    '''
    MAIN

    Executes three diferent outputs:
        - `description --data --output`: Description of input dataset
        - `train --data --output`: Train model over dataset (s,r)
        - `test --data --output --model-path `: Test with trained `model`, over input dataset
    '''
    config = retrieve_config()

    # environment
    env = None
    num_frames = 0
    memory_size = 0
    batch_size = 0
    target_update = 0
    epsilon_decay = 0

    if config['env'] == 'cartpole':
        env = CartPole()
        num_frames = 1000
        memory_size = 1000
        batch_size = 5
        target_update = 100
        epsilon_decay = 1 / 2000
        ####################
        # Parameters for RAINBOW
        gamma = 0.2
        # PER parameters
        alpha = 0.2
        beta = 0.6
        prior_eps = 1e-6
        # Categorical DQN parameters
        v_min = 0.0
        v_max = 200.0
        atom_size = 51
    
    elif config['env'] == 'kdd99':
        obs_per_episode = 10
        total_obs = 250_000
        if config['command'] == 'test':
            # TODO read metadata.json and get 'total_obs'
            skiprows = total_obs
            
        else:
            skiprows=0
        
        env = KDD99(
            total_obs=total_obs,
            states_per_episode=obs_per_episode,
            skiprows=skiprows
        )
        
        num_frames = total_obs
        memory_size = 1000
        batch_size = 5
        target_update = 100
        epsilon_decay = 1 / 2000
        gamma = 0.2
        ####################
        # Parameters for RAINBOW
        # PER parameters
        alpha = 0.2
        beta = 0.6
        prior_eps = 1e-6
        # Categorical DQN parameters
        v_min = -float(obs_per_episode)
        v_max = float(obs_per_episode)
        atom_size = obs_per_episode*2
        
    else:
        print("Environment non exsiting")
        sys.exit()

    # Agent
    agent = None
    if config['agent'] == 'dqn':
        # Create Agent
        agent = dqn_agent.DQNAgent(
            env,
            memory_size = memory_size,
            batch_size = batch_size,
            target_update = target_update,
            epsilon_decay = epsilon_decay
        )
    elif config['agent'] == 'rainbow':
        agent = rainbow_agent.DQNAgent(
            env,
            memory_size = memory_size,
            batch_size = batch_size,
            target_update= target_update,
            gamma = gamma,
            # PER parameters
            alpha = alpha,
            beta = beta,
            prior_eps = prior_eps,
            
            # Categorical DQN parameters
            v_min = v_min,
            v_max = v_max,
            atom_size = atom_size,
            
            # # N-step Learning
            n_step = 1
        )
        # sys.exit()
    else:
        print(f'Agent {config["agent"]} does not exist')
        sys.exit()


    # Mode/Action (train, test, ...)
    if config['command'] == 'train':
        #agent.load_model()
        
        agent.train(num_frames = num_frames)
        
        if config['save'] is not None:
            path = ""
            if config['save'] is True:
                path = agent.save_model(path=config['save'])
            else:
                path = agent.save_model(path=config['save'])
            print(f'Output saved at \n"{path}"')
        else:
            pass

    elif config['command'] == 'test':
        root_output = Path(__file__).parent.parent/'output'
        if config['model'] is not None:
            path = ""
            if config['model'] is True:
                # Argument `-m` with no value
                path = root_output/config['agent']/config['env']/latest 
                model_path = path/'model.pt' 
                # If no argument `-m` 
                path = agent.load_model(path=model_path)
            else:
                # Argument `-m path.pt` with value
                path = agent.load_model(path=config['model'])
            print(f'Output loaded from \n"{path}"')

        else:
            path = root_output/config['agent']/config['env']/latest 
            model_path = path/'model.pt' 
            # If no argument `-m` 
            path = agent.load_model(path=model_path)

        agent.test()

        sys.exit()
    else:
        print(f'Command {config["command"]} does not exist')
        sys.exit()


"""
python src -a="rainbow" -e="kdd99" train -s . 
"""