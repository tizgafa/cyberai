import pytest
import numpy as np
from ..simple_replay_buffer.simple_replay_buffer import ReplayBuffer

def test_store():
  size = 10
  buffer = ReplayBuffer(
    obs_dim=2,
    size=size,
    batch_size= 2
  )
  
  assert len(buffer) == 0

  for i in range(0,size):
    assert _assert_transition_in_store(
      buffer=buffer,
      ptr = i % size,
      length= min(i+1, size),
      obs = np.array([i,i]),
      act = 1,
      rew = 0,
      next_obs = np.array([i+1,i+1]),
      done = True
    )

  assert _assert_transition_in_store(
    buffer=buffer,
    ptr = 0,
    length = size,
    obs = np.array([i,i]),
    act = 1,
    rew = 0,
    next_obs = np.array([i+1,i+1]),
    done = True
  )
  

  assert 1==1


def _assert_transition_in_store(buffer, ptr, length, obs, act, rew, next_obs, done):
  buffer.store(
    obs = obs,
    act = act,
    rew = rew,
    next_obs = next_obs,
    done = done
  )
  # print("\nSize", len(buffer))
  # print("obs", buffer.obs_buf[ptr])
  # print("next obs", buffer.next_obs_buf[ptr])
  # print("action", buffer.acts_buf[ptr])
  # print("reward", buffer.rews_buf[ptr])
  # print("done", buffer.done_buf[ptr])

  assert len(buffer) == length
  assert _equal_np_arrays(buffer.obs_buf[ptr], obs)
  assert buffer.acts_buf[ptr] == act
  assert buffer.rews_buf[ptr] == rew
  assert _equal_np_arrays(buffer.next_obs_buf[ptr], next_obs)
  assert buffer.done_buf[ptr] == done
  return True

def _equal_np_arrays(actual, expected):
  assert len(actual) == len(expected)
  assert all([a == b for a, b in zip(actual, expected)])
  return True